# Flawfinder analyzer changelog

## v2.13.0

- Expose the flawfinder errors when it fails to run (!51)
- Output help text if a character encoding issue is encountered (!51)

## v2.12.1
- Update from flawfinder 2.0.11 to 2.0.15 (!49 @thomas-nilsson-irfu)
  - 2.0.15
    - Improved handling of LoadLibraryEx; flawfinder no longer complains
      about certain constructs that are known to be safe (eliminating
      some false positives).
  - 2.0.14
    - Various Windows improvments.
      Ignore LoadLibraryEx if its third parameter is
      LOAD_LIBRARY_SEARCH_SYSTEM32, as this is safe, and
      remove the rule for InitialCriticalSection
      (this is no longer a vulnerability on current widely-used versions
      of Windows)
    - Various C++ improvements.  Add .hpp support for C++,
      ignore "system::" to reduce false positives,
      treat ' as digit separator when file extension is a C++ file
      (for C++14).

## v2.12.0
- Update common to v2.22.0 (!47)
- Update urfave/cli to v2.3.0 (!47)

## v2.11.1
- Update logrus and cli golang dependencies (!46)

## v2.11.0
- Update common and enable disablement of custom rulesets(!44)

## v2.10.3
- Update golang dependencies (!38)

## v2.10.2
- Update golang dependencies (!35)

## v2.10.1
- Reclassify confidence level as severity (!32)

## v2.10.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!35)

## v2.9.1
- Upgrade go to version 1.15 (!33)

## v2.9.0
- Add scan object to report (!29)

## v2.8.0
- Switch to the MIT Expat license (!26)

## v2.7.1
- Update Debug output to give a better description of command that was ran (!25)

## v2.7.0
- Update logging to be standardized across analyzers (!24)

## v2.6.1
- Remove `location.dependency` from the generated SAST report (!23)

## v2.6.0
- Use alpine as base image (!18)

## v2.5.0
- Bump Flawfinder to [2.0.11](https://sourceforge.net/p/flawfinder/code/ci/2.0.11/tree/ChangeLog) (!21)

## v2.4.0
- Add `id` field to vulnerabilities in JSON report (!17)

## v2.3.0
- Add support for custom CA certs (!15)

## v2.2.1
- Use Debian Stretch as base image (!11)

## v2.2.0
- Bump Flawfinder to [2.0.10](https://sourceforge.net/p/flawfinder/code/ci/2.0.10/tree/ChangeLog)

## v2.1.0
- Bump Flawfinder to [2.0.8](https://sourceforge.net/p/flawfinder/code/ci/2.0.8/tree/ChangeLog)

## v2.0.1
- Bump common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.4.0
- Add an `Identifier` generated from the Flawfinder's function name

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
